# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kwidgetsaddons
pkgver=6.2.1
pkgrel=0
pkgdesc="Addons to QtWidgets"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="GPL-2.0-only AND LGPL-2.1-only AND Unicode-DFS-2016"
depends_dev="qt6-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt6-qttools-dev
	samurai
	"
checkdepends="
	mesa-dri-gallium
	xvfb-run
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/frameworks/kwidgetsaddons.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwidgetsaddons-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# ktwofingertaptest, ktwofingerswipetest and and ksqueezedtextlabelautotest are broken
	xvfb-run ctest --test-dir build --output-on-failure -E '(ktooltipwidget|ktwofingertap|ktwofingerswipe|ksqueezedtextlabelauto)test'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4b2e35f814d00c99e5b41c21171140c64aceeba9da2075a7725027ae5c9721d8b0e008369a79672a1076ac49f901e01b1d8319ff9e4d894cab7f6b7592b4ed3f  kwidgetsaddons-6.2.1.tar.xz
"
