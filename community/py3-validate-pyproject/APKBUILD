# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-validate-pyproject
pkgver=0.17
pkgrel=0
pkgdesc="Validation library for simple check on pyproject.toml"
url="https://validate-pyproject.readthedocs.io"
arch="noarch"
license="MPL-2.0"
depends="
	py3-fastjsonschema
	python3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="py3-pytest-xdist"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/abravalheri/validate-pyproject/archive/refs/tags/v$pkgver.tar.gz
	no-useless-check.patch
	"
builddir="$srcdir/validate-pyproject-$pkgver"
options="net" # tests require network

build() {
	gpep517 build-wheel	\
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto -k 'not test_downloaded and not test_private_classifier'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
8e7e38eee09144c123f0f971f386b39bad0ac66b179db57f95195abe902a4fbaef81f33f8c2eccdc1e63e0c11b025dd11ef50b5d20da4779b10ed046b415c501  py3-validate-pyproject-0.17.tar.gz
831c908ac5c8d62bafe8aafff2ae27998d568e0479f90ed721493c0c4731d1914bb457055ca3dfdf97c7a780939e2027fcd8359339258c8e82a226d5a22da967  no-useless-check.patch
"
