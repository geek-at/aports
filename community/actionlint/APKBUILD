# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=actionlint
pkgver=1.7.0
pkgrel=1
pkgdesc="Static checker for GitHub Actions workflow files"
url="https://github.com/rhysd/actionlint"
arch="all"
license="MIT"
makedepends="go ronn"
options="chmod-clean net"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/rhysd/actionlint/archive/refs/tags/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-ldflags="-buildid= -X github.com/rhysd/actionlint.version=$pkgver -s -w" \
		./cmd/$pkgname
	ronn ./man/$pkgname.1.ronn
}

check() {
	go test .
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname
	install -Dm644 man/$pkgname.1 "$pkgdir"/usr/share/man/man1/$pkgname.1
	install -Dm644 LICENSE.txt "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
5d465c8f87c205f2a727f90a972ecd8ef345f56313990598714d07ef208a9b9cf868a6f3ba211ccc9ff102e93cb99e39109f0473bd1a33ea4961681a2dd549f5  actionlint-1.7.0.tar.gz
"
